def calculate(operator, first, second):
    match operator:
        case '+':
            return(first + second)
        case '-':
            return(first - second)
        case '*' | 'x':
            return(first * second)
        case '/':
            return(first / second)
        case _:
            return(None)



file = open("step_3.txt", "r")

fileLines = file.read().splitlines()

currentLineIndex = 1

statements = []

while(True):
    currentLine = fileLines[currentLineIndex - 1]
    print(currentLine)

    if (currentLine in statements):
        break

    splitLine = currentLine.split(" ")

    if (len(splitLine) == 2):
        currentLineIndex = int(splitLine[1])
    else:    
        operator = splitLine[2]
        first = int(splitLine[3])
        second = int(splitLine[4])
        currentLineIndex = int(calculate(operator, first, second))

    print(currentLineIndex)

    statements.append(currentLine)


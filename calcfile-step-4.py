# an update

def calculate(operator, first, second):
    match operator:
        case '+':
            return(first + second)
        case '-':
            return(first - second)
        case '*' | 'x':
            return(first * second)
        case '/':
            return(first / second)
        case _:
            return(None)



file = open("step_4.txt", "r")
fileLines = file.read().splitlines()

currentLineIndex = 1

statements = []

while(True):

    currentLine = fileLines[currentLineIndex - 1]
    print(currentLine)

    if (currentLine in statements):
        break

    splitLine = currentLine.split(" ")

    command = splitLine[0]

    match command:
        case 'goto':

            if (splitLine[1] == 'calc'):
                operator = splitLine[2]
                first = int(splitLine[3])
                second = int(splitLine[4])
                currentLineIndex = int(calculate(operator, first, second))

            else:
                currentLineIndex = int(splitLine[1])

            print("Next line ", currentLineIndex)


        case 'replace':
            lineToRemove = int(splitLine[1])
            lineToInsert = int(splitLine[2])
            line = fileLines[lineToInsert - 1]
            fileLines[lineToRemove - 1] = line


        case 'remove':
            lineToRemove = int(splitLine[1])
            if (lineToRemove <= len(fileLines)):
                print("Removing line ", lineToRemove)
                del fileLines[lineToRemove - 1]

    print(currentLineIndex)
    statements.append(currentLine)


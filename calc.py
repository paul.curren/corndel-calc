from email.policy import default


operation = input("Operation: ")
first = int(input("First value: "))
second = int(input("Second value: "))

match operation:
    case '+':
        print(first + second)
    case '-':
        print(first - second)
    case '*' | 'x':
        print(first * second)
    case '/':
        print(first / second)
    case _:
        print("Invalid operation")





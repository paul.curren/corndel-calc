def calculate(operator, first, second):
    match operator:
        case '+':
            return(first + second)
        case '-':
            return(first - second)
        case '*' | 'x':
            return(first * second)
        case '/':
            return(first / second)
        case _:
            return(None)



file = open("step_2.txt", "r")
total = 0
for line in file:
    splitLine = line.split(" ")
    operator = splitLine[1]
    first = int(splitLine[2])
    second = int(splitLine[3])
    total = total + calculate(operator, first, second)

print("Total = ", total)
